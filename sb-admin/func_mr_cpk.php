<?php
function cpk_part($new_tipe,$new_mtc,$new_repair,$mileage,$unit,$contract) {
include "connectdatabase.php";

$sub_part = $new_mtc + $new_repair;
$pembagi = $mileage * 12 * $contract * $unit;
$cpk = $sub_part / $pembagi;
?>
<!--<div class="row">-->
<!---------- CONTENT MASTER UNIT --------------->
	<div class="col-lg-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-car"></i> CPK Parts & Repairs - <?php echo $new_tipe; ?></h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr class="bg-primary"><th>Item</th><th>Sub Total Costs</th></tr>
						</thead>
						<tbody>
							<tr><td><a href="#" data-toggle="modal" data-target = "#myModal">Preventive Maintenance Parts</a></td><td><?php echo number_format($new_mtc); ?></td></tr>
							<tr><td><a href="#">Periodic Repairs</a></td><td><?php echo number_format($new_repair); ?></td></tr>
						</tbody>
						<tfoot>
							<tr class="bg-warning"><td>Total Costs</td><td><?php echo number_format($sub_part); ?></td></tr>
							<tr class="bg-warning"><td>CPK</td><td><?php echo number_format($cpk); ?></td></tr>
						</tfoot>
					</table>
				</div>
				<div class="text-right">
			
				</div>				
			</div>
		</div>
	</div>
<!--</div><!-- /.row -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
   
	<div class = "modal-dialog modal-lg">

	<div class = "modal-content">
	<div class = "modal-header">
	</div>

	<div class = "modal-body">	
	</div>
			
	<div class = "modal-footer">
		<button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
	</div>
	         
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
			  
	</div><!-- /.modal -->	
<?php 
Return $cpk;
}
?>