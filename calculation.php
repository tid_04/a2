<!doctype html>
<html>

<head>    
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
   <!--<meta charset='utf-8'>-->  
	<meta http-equiv="X-UA-Compatible" content="IE=edge">    
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
	<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
	<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<style type="text/css">
.table-borderless > tbody > tr > td {
    vertical-align: middle;
}
.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
    border: none;
}
</style>

<body>

<form action="processing.php" method="post">
<div class="row">
	<div class="col-lg-6">
	<center><h3>SIMULASI</h3></center>
	</div>
</div>
<div class="row">
	<div class="col-lg-3">
		<div class="panel panel-info">
			<div class="panel-heading">Customer Information</div>
			<div class="panel-body">
                <div class="table-responsive">
					<table class="table table-borderless table-hover">
					<tr><td>Disc. (%)</td><td colspan=2><input type="number" name="disc" id="disc" class="form-control" value = 0 autofocus></td></tr>	
					<tr><td>Type</td>
					<td colspan=2><select name="tipe" class="form-control" autofocus required><option value="">Select Type .. </option>
					<?php
					include "connectdatabase.php";
					$part = mysqli_query($conn,"select * from unitmaster where valuable = 1");
					while ($pCol = mysqli_fetch_array($part)) {
						echo "<option value='$pCol[1]'>$pCol[2]</option>";
					}
					?>
					</select></td></tr>		
					<tr><td>Jumlah Unit</td><td colspan=2><input type="number" name="jumlah" class="form-control" required></td></tr>
					<tr><td>Jarak Tempuh Sehari</td><td><input type="text" name="jarak" class="form-control" required></td><td style="valign: center">Km/hari</td></tr>
					<tr><td>Working Days</td><td><input type="number" name="day" class="form-control" required></td><td style="valign: center">Hari</td></tr>
					<tr><td>Lama Kontrak</td><td><input type="number" name="kontrak" class="form-control" required></td><td>Tahun</td></tr>		
					</table>
				</div>
				<div class="text-right">

                </div>
			</div>
			<!--<div class="panel-footer">
				<button type="submit" class="btn btn-primary">PROCESS</button>
			</div>-->
		</div>
		<!--</form>-->
	</div>

	<div class="col-lg-3">
		<form action="processing.php" method="post">
		<div class="panel panel-info">
			<div class="panel-heading">Men Power</div>
			<div class="panel-body">
                <div class="table-responsive">
					<table class="table table-hover table-borderless">
					<tr><td>Senior Mechanics</td><td><input type="number" name="senior" id="senor" class="form-control" value = 0 autofocus></td></tr>	
					<tr><td>Junior Mechanics</td><td><input type="number" name="junior" id="junior" class="form-control" value = 0 autofocus></td></tr>	
					</table>
				</div>
				
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary">PROCESS</button>
			</div>
		</div>
		<!--</form>-->
	</div>
</div><!-- /.row -->
</form>
</body>
</html>
		