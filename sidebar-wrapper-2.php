<!doctype HTML>
<html>

<head>    
	<meta charset="utf-8">    
	<meta http-equiv="X-UA-Compatible" content="IE=edge">    
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
	<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
	<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href = "css/sidebar-wrapper-2.css" rel = "stylesheet">
	<script src = "js/sidebar-wrapper-2.js"></script>
</head>

<body>
  <nav class="navbar navbar-default no-margin">
    
    <div class="navbar-header fixed-brand">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" id="menu-toggle">
        <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
      </button>
      <a class="navbar-brand" href="#">SEEGATESITE</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active">
          <button class="navbar-toggle collapse in" data-toggle="collapse" id="menu-toggle-2"> <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span></button>
        </li>
      </ul>
    </div>
    <!-- navbar-header-->


    <!-- bs-example-navbar-collapse-1 -->
  </nav>
  <div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav nav-pills nav-stacked" id="menu">

        <li class="active">
          <a href="#"><span class="glyphicon glyphicon-dashboard"></span>&nbsp; Dashboard</a>
          <!--<ul class="nav-pills nav-stacked" style="list-style-type:none;">
            <li><a href="#">link1</a></li>
            <li><a href="#">link2</a></li>
          </ul>-->
        </li>
        <!--<li>
          <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span> Shortcut</a>
          <ul class="nav-pills nav-stacked" style="list-style-type:none;">
            <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link1</a></li>
            <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span>link2</a></li>

          </ul>
        </li>-->
        <li>
          <a href="#"><span class="glyphicon glyphicon-usd"></span>&nbsp; Calculation</a>
        </li>
        <li>
          <a href="#"> <span class="glyphicon glyphicon-random"></span>&nbsp; View Process</a>
        </li>
        <li>
          <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-youtube-play fa-stack-1x "></i></span>About</a>
        </li>
        <li>
          <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span>Services</a>
        </li>
        <li>
          <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Contact</a>
        </li>
      </ul>
    </div>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <!----------- CONTENT ---------------------->

          </div>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->
  <!-- jQuery -->
</body>