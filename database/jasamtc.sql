-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2020 at 02:07 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jasamtc`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_general`
--

CREATE TABLE `inv_general` (
  `inv_id` int(11) NOT NULL,
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_general`
--

INSERT INTO `inv_general` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Tools Set (STANLEY)', 7292333, 1),
(2, 'Tool Box (STANLEY)', 1409500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inv_measuring`
--

CREATE TABLE `inv_measuring` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_measuring`
--

INSERT INTO `inv_measuring` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Multi Tester (SANWA)', 675000, 1),
(2, 'Vernier Caliper (MITUTOYO)', 2750000, 1),
(3, 'Engine Oil Pressure Gauge (NPA)', 2650000, 1),
(4, 'Tire Pressure Gauge (CHINA)', 290000, 2),
(5, 'Hydrometer (CHINA)', 50000, 1),
(6, 'Diesel Compression gauge (NPA)', 8520000, 1),
(7, 'Radiator Cap Tester (NPA)', 4000000, 1),
(8, 'Torque Wrench (Tohnichi 6 -42 Kgm)', 5700000, 1),
(9, 'Torque Wrench (Tohnichi 10 - 55 Kgm)', 8500000, 1),
(10, 'Torque Wrench (Tohnichi 10-100 Kgm)', 12200000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inv_others`
--

CREATE TABLE `inv_others` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_others`
--

INSERT INTO `inv_others` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Stand Axle FR', 3900000, 1),
(2, 'Stand Axle RR', 3900000, 1),
(3, 'Air Compressor (SWAN 15 HP)', 46250000, 1),
(4, 'Wheel stopper ', 600000, 4),
(5, ' Lap Top', 5000000, 1),
(6, 'Service Car (Dutro)', 500000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inv_power`
--

CREATE TABLE `inv_power` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_power`
--

INSERT INTO `inv_power` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Engine Lifting Sling', 2750000, 1),
(3, 'Air Gun (EJN)', 360000, 2),
(4, 'Air Impact 3/4 inchi (OBASE)', 5199400, 1),
(5, 'Air Impact 1/2 inchi (OBASE)', 2260100, 1),
(6, 'Air Hose Rail (TOYOX)', 3000000, 1),
(7, 'Manual Pump For Drum (Engine Oil) (OREINTAL)', 300000, 1),
(8, 'Manual Pump For Drum (Transmission Oil) (OREINTAL)', 300000, 1),
(9, 'Manual Pump For Drum (Differential) (OREINTAL)', 300000, 1),
(10, 'Chain Block 2 T X 3 Meter (NITCHI)', 3500000, 1),
(11, 'Nozzle Tester (Bosch)', 5011600, 1),
(12, 'Electronic Soldering Iron (150 Watt) (GOOT)', 900000, 1),
(13, 'Pneumatic Grease Pump (FUGIMAKU)', 5512500, 2),
(14, 'Pneumatic Oil Pump (FUGIMAKU)', 6250000, 2),
(15, 'Pneumatic Rivet Press', 75000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inv_safety`
--

CREATE TABLE `inv_safety` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_safety`
--

INSERT INTO `inv_safety` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Fire Extingiusher', 5000000, 1),
(2, 'Grinding Geogle', 15000, 3),
(3, 'Welding Gogle', 15000, 1),
(4, 'Hand Glove', 15000, 3),
(5, 'String', 125000, 3),
(6, 'Mask Protector', 15000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `inv_sst_dutro`
--

CREATE TABLE `inv_sst_dutro` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_sst_dutro`
--

INSERT INTO `inv_sst_dutro` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Fuel Filter Wrench (09228LEA10)', 5238000, 1),
(2, 'Oil Filter Wrench (S095531010)', 335000, 1),
(3, 'Front Hub Oil Seal Press (0995100910L)', 295000, 1),
(4, 'Front Hub Oil Seal Press (0995107150L)', 227000, 1),
(5, 'Rear Hub Oil Seal Press (0951712011L)', 451000, 1),
(6, 'Rear Hub & Drum Puller (09510LGA10)', 574000, 1),
(7, 'Rear Axle Bearing Lock Nut Wrench (0951336020L)', 278000, 1),
(8, 'Brake Shoe Return Spring (0970330011)', 435000, 1),
(9, 'Shoe Hold Down Spring Driver (0971800011)', 225000, 1),
(10, 'Clutch Guide Tool (0930100120L)', 182000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inv_sst_ranger`
--

CREATE TABLE `inv_sst_ranger` (
  `inv_id` int(11) NOT NULL DEFAULT '0',
  `inv_item` varchar(100) NOT NULL,
  `inv_price` int(11) NOT NULL,
  `inv_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_sst_ranger`
--

INSERT INTO `inv_sst_ranger` (`inv_id`, `inv_item`, `inv_price`, `inv_qty`) VALUES
(1, 'Oil filter wrench 09503LBA90H', 891000, 1),
(2, 'Fuel filter wrench 09503LBA40H', 715000, 1),
(3, 'I/P coupling wrench 09819LBH07', 165000, 1),
(4, 'Arbor 09662LBC00 ', 410000, 1),
(5, 'Arbor 09662LBA80', 344000, 1),
(6, 'Release lever height gauge 09661LBA30', 461600, 1),
(7, 'Release lever height gauge 09661LBA20', 384700, 1),
(8, 'Pilot bearing puller 09650LBJ70', 688000, 1),
(9, 'Pilot bearing puller 096501020L', 1128300, 1),
(10, 'Socket wrench nut wheel hub 098397001L', 886300, 1),
(11, 'Screw puller 09652LBC10', 77000, 1),
(12, 'Wheel hub puller 09650LBC92', 817000, 1),
(13, 'Plate S096541520', 1486000, 1),
(14, 'Steering wheel puller 09650LBD41', 475000, 1),
(15, 'Pitman arm puller 09650LBC60', 975000, 1),
(16, 'Wheel hub puller 096501790L', 5307700, 1),
(17, 'Socket wrench nut wheel hub 09839LFF01', 770000, 1),
(18, 'Socket wrench nut wheel hub 096031360L', 2097000, 1),
(19, 'Puller wheel hub bearing 096501310L', 6269300, 1),
(20, 'Retracting spring remover 09606LBA40', 163000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `fullname` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `fullname`) VALUES
('ADMIN', '12345', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `manpower`
--

CREATE TABLE `manpower` (
  `mp_id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `mek_senior` int(11) NOT NULL,
  `mek_junior` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manpower`
--

INSERT INTO `manpower` (`mp_id`, `item`, `mek_senior`, `mek_junior`) VALUES
(1, 'Bsc Salary', 5070000, 4420000),
(2, 'Laundry ', 250000, 250000),
(3, 'Transport Allowance', 170000, 170000),
(4, 'Mobile phone', 200000, 200000),
(5, 'Uniform and Safety', 370000, 370000),
(6, 'Hardship ', 2500000, 2500000),
(7, 'Overtime', 1435632, 1435632),
(8, 'Daily Meal', 60000, 60000),
(9, 'PPH', 0, 0),
(10, 'Meal Allowance', 0, 0),
(11, 'Medical Allowance', 0, 0),
(12, 'BPJS Kesehatan and T. Kerja', 0, 0),
(13, 'THR', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `manpower_note`
--

CREATE TABLE `manpower_note` (
  `item` varchar(50) NOT NULL,
  `rumus` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manpower_note`
--

INSERT INTO `manpower_note` (`item`, `rumus`) VALUES
('PPH', '(SALARY + THR) X 5%'),
('MEAL ALLOWANCE', 'DAILY MEAL X 30 DAYS'),
('MEDICAL ALLOWANCE', '(SALARY X 2) / 12 MONTH'),
('BPJS KESEHATAN + T. KERJA', 'SALARY X 11%'),
('THR', 'SALARY / 12 MONTH');

-- --------------------------------------------------------

--
-- Table structure for table `partmaster_mtc`
--

CREATE TABLE `partmaster_mtc` (
  `partmtc_id` int(11) NOT NULL,
  `part_name` varchar(100) NOT NULL,
  `part_no` varchar(20) NOT NULL,
  `pricelist` int(11) NOT NULL,
  `km1th` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partmaster_mtc`
--

INSERT INTO `partmaster_mtc` (`partmtc_id`, `part_name`, `part_no`, `pricelist`, `km1th`) VALUES
(1, 'Element Oil Filter', '15607-2190L', 158000, 60000),
(2, 'Element Oil Filter Differential', '15607-2060L', 41500, 60000),
(3, 'Element Fuel Filter Upper', '23401LBH30', 95500, 60000),
(4, 'Element Fuel Filter Lower', '23390JAA10', 166000, 60000),
(5, 'Element Air Cleaner Out', '17801-3380L', 408000, 60000),
(6, 'Element Air Cleaner In', '17801-3391L', 155000, 60000),
(7, 'Engine Oil (HGO)(04100-1001)', '041001001', 53000, 60000),
(8, 'Transmission Oil (HGO 80W- 90) GL - 5 (S041024414 )', 'S041024414(T)', 77000, 60000),
(9, 'Differential Oil  (HGO 80W- 90) GL - 5 (S041024414 )', 'S041024414(D)', 77000, 60000),
(10, 'Clutch Fluid(041101002L) 300 ML', '041101002L', 28000, 60000),
(11, 'Brake Fluid(041101001L) 1 LTR', '041101001L', 81400, 60000),
(12, 'Power steering Oil ( Hino Genuine PSF ) (0410721042)', '0410721042', 85000, 60000),
(13, 'Hino Genuine Bearing Grease  (S041040024 )', 'S041040024', 102000, 60000),
(14, 'Chassis Grease (Pertamina SGX)', 'GREASE-PTMSGX', 41500, 60000),
(15, 'Element Oil Filter', '15613JAA10', 183000, 60000),
(16, 'Element Oil Filter Transmission', '32915-LVA10', 40400, 60000),
(17, 'Element Fuel Filter Upper', '23414-LAA10', 44700, 60000),
(18, 'Element Fuel Filter Lower', '23304JAC70', 92800, 60000),
(19, 'Element Air Cleaner Out', '17801JAA10', 448000, 60000),
(20, 'Element Air Cleaner In', '17801JAA20', 158000, 60000);

-- --------------------------------------------------------

--
-- Table structure for table `partmaster_rpr`
--

CREATE TABLE `partmaster_rpr` (
  `partrpr_id` int(11) NOT NULL,
  `part_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `part_no` varchar(20) CHARACTER SET utf8 NOT NULL,
  `pricelist` int(11) NOT NULL,
  `km1th` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partmaster_rpr`
--

INSERT INTO `partmaster_rpr` (`partrpr_id`, `part_name`, `part_no`, `pricelist`, `km1th`) VALUES
(1, 'Brake Lining Fr', '04477JAE50', 82000, 75884),
(2, 'Brake Lining Rr', '04477JAE60', 115000, 75884),
(3, 'Radiator', '16400EW010', 3498000, 75884),
(4, 'Supply Pump Assy', '22100E0430', 46896000, 75884),
(5, 'Alternator', '27060EW041', 35704000, 75884),
(6, 'Motor Starter', '28100EW051', 13078000, 75884),
(7, 'Clutch Cover Assy', '31210E0241', 6135000, 75884),
(8, 'Disc Clutch', '31250JAE90', 2828000, 75884),
(9, 'Front Leaf Sub Assy Spring No.1 ', '48101EV020', 1622000, 75884),
(10, 'Front Leaf Spring No.2', '48112EV020', 1129000, 75884),
(11, 'Rear Leaf Spring No.1', '48211EV040', 1540000, 75884),
(12, 'Rear Leaf Spring No.2', '48212EV040', 1480000, 75884),
(13, 'Rear Leaf Spring No.3', '48213EV040', 1500000, 75884),
(14, 'Shock Absorber Fr LH-RH', '48500EW020', 1709000, 75884),
(15, 'Torque Bushing FR Side', '493051110L(Fr)', 198000, 75884),
(16, 'Torque Bushing RR Side', '493051110L(Rr)', 198000, 75884),
(17, 'Front Wheel Bearing Inner', '988465116L', 1005000, 75884),
(18, 'Thermostat', 'S1632E9000', 829000, 75884),
(19, 'Rivet Kit Fr', 'S407MEV071(Fr)', 1761000, 75884),
(20, 'Rivet Kit Rr', 'S407MEV071(Rr)', 1761000, 75884),
(21, 'Repair Kit Air Brake Valve', 'S470691030', 1682000, 75884),
(22, 'Front Leaf Sub Assy Spring No.3', 'S4803EV020', 755000, 75884),
(23, 'O Ring', 'SZ30165006', 13000, 75884),
(24, 'Oil Seal', 'SZ31101046', 98100, 75884),
(25, 'Rear Wheel Oil Seal Inner', 'SZ31101047(Rr)', 132000, 75884),
(26, 'Rearward Wheel Oil Seal Inner', 'SZ31101047(Rw)', 132000, 75884),
(27, 'Rear Wheel Oil Seal Outer', 'SZ31176002(Rr)', 151000, 75884),
(28, 'Rearward Wheel Oil Seal Outer', 'SZ31176002(Rw)', 151000, 75884),
(29, 'Front Wheel Bearing Outer', 'SZ36650014L', 539000, 75884),
(30, 'Rear Wheel Bearing Inner', 'SZ366JAA10(Rr)', 1139000, 75884),
(31, 'Rearward Wheel Bearing Inner', 'SZ366JAA10(Rw)', 1139000, 75884),
(32, 'Rear Wheel Bearing Outer', 'SZ366JFA11(Rr)', 985000, 75884),
(33, 'Rearward Wheel Bearing Outer', 'SZ366JFA11(Rw)', 985000, 75884),
(34, 'Pilot Bearing', 'SZ37125045', 263000, 75884),
(35, 'V-Belt', 'SZ91049333', 962000, 75884),
(36, 'Gasket', 'SZ93021100', 68000, 75884),
(37, 'Bulb Lamp Rr. RH 24 V - 25/10 W', 'SZ98032070', 270000, 75884),
(38, 'Bulb Lamp Rr. LH 24 V - 21 W', 'SZ98032071', 298000, 75884),
(39, 'Bulb Head Lamp Fr. RH 24 V - 75/70 W', 'SZ980T7001(R)', 229000, 75884),
(40, 'Bulb Head Lamp Fr. LH 24 V - 75/70 W', 'SZ980T7001(L)', 229000, 75884);

-- --------------------------------------------------------

--
-- Table structure for table `part_mtc`
--

CREATE TABLE `part_mtc` (
  `typemtc_id` int(11) NOT NULL,
  `unit_type` varchar(20) NOT NULL,
  `part_no` varchar(20) NOT NULL,
  `qty` decimal(11,2) NOT NULL,
  `kminterval` int(11) NOT NULL,
  `probabilitas` decimal(11,2) NOT NULL,
  `discincl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `part_mtc`
--

INSERT INTO `part_mtc` (`typemtc_id`, `unit_type`, `part_no`, `qty`, `kminterval`, `probabilitas`, `discincl`) VALUES
(1, 'FM285TH', '15607-2190L', '1.00', 20000, '1.00', 1),
(2, 'FM285TH', '15607-2060L', '1.00', 60000, '1.00', 1),
(3, 'FM285TH', '23401LBH30', '1.00', 10000, '1.00', 1),
(4, 'FM285TH', '23390JAA10', '1.00', 10000, '1.00', 1),
(5, 'FM285TH', '17801-3380L', '1.00', 50000, '1.00', 1),
(6, 'FM285TH', '17801-3391L', '1.00', 50000, '1.00', 1),
(7, 'FM285TH', '041001001', '12.70', 20000, '1.00', 1),
(8, 'FM285TH', 'S041024414(T)', '9.30', 60000, '1.00', 1),
(9, 'FM285TH', 'S041024414(D)', '24.00', 60000, '1.00', 1),
(10, 'FM285TH', '041101002L', '0.30', 60000, '1.00', 1),
(11, 'FM285TH', '041101001L', '1.10', 60000, '1.00', 1),
(12, 'FM285TH', '0410721042', '3.40', 60000, '1.00', 1),
(13, 'FM285TH', 'S041040024', '6.40', 60000, '1.00', 1),
(14, 'FM285TH', 'GREASE-PTMSGX', '2.00', 10000, '1.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `part_rpr`
--

CREATE TABLE `part_rpr` (
  `typerpr_id` int(11) NOT NULL,
  `unit_type` varchar(20) NOT NULL,
  `part_no` varchar(20) NOT NULL,
  `qty` decimal(11,2) NOT NULL,
  `kminterval` int(11) NOT NULL,
  `probabilitas` decimal(11,2) NOT NULL,
  `discincl` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `part_rpr`
--

INSERT INTO `part_rpr` (`typerpr_id`, `unit_type`, `part_no`, `qty`, `kminterval`, `probabilitas`, `discincl`) VALUES
(1, 'FM285TH', '04477JAE50', '8.00', 30000, '1.00', 1),
(2, 'FM285TH', '04477JAE60', '16.00', 30000, '1.00', 1),
(3, 'FM285TH', '16400EW010', '1.00', 300000, '0.30', 1),
(4, 'FM285TH', '22100E0430', '1.00', 120000, '0.50', 1),
(5, 'FM285TH', '27060EW041', '1.00', 300000, '0.30', 1),
(6, 'FM285TH', '28100EW051', '1.00', 300000, '0.30', 1),
(7, 'FM285TH', '31210E0241', '1.00', 120000, '1.00', 1),
(8, 'FM285TH', '31250JAE90', '1.00', 60000, '1.00', 1),
(9, 'FM285TH', '48101EV020', '2.00', 60000, '1.00', 1),
(10, 'FM285TH', '48112EV020', '2.00', 60000, '1.00', 1),
(11, 'FM285TH', '48211EV040', '2.00', 60000, '1.00', 1),
(12, 'FM285TH', '48212EV040', '2.00', 60000, '1.00', 1),
(13, 'FM285TH', '48213EV040', '2.00', 60000, '1.00', 1),
(14, 'FM285TH', '48500EW020', '2.00', 120000, '0.50', 1),
(15, 'FM285TH', '493051110L(Fr)', '6.00', 120000, '1.00', 1),
(16, 'FM285TH', '493051110L(Rr)', '6.00', 120000, '1.00', 1),
(17, 'FM285TH', '988465116L', '2.00', 60000, '1.00', 1),
(18, 'FM285TH', 'S1632E9000', '1.00', 300000, '0.30', 1),
(19, 'FM285TH', 'S407MEV071(Fr)', '2.00', 30000, '1.00', 1),
(20, 'FM285TH', 'S407MEV071(Rr)', '4.00', 30000, '1.00', 1),
(21, 'FM285TH', 'S470691030', '1.00', 60000, '0.50', 1),
(22, 'FM285TH', 'S4803EV020', '2.00', 60000, '1.00', 1),
(23, 'FM285TH', 'SZ30165006', '2.00', 60000, '1.00', 1),
(24, 'FM285TH', 'SZ31101046', '2.00', 60000, '1.00', 1),
(25, 'FM285TH', 'SZ31101047(Rr)', '2.00', 60000, '1.00', 1),
(26, 'FM285TH', 'SZ31101047(Rw)', '2.00', 60000, '1.00', 1),
(27, 'FM285TH', 'SZ31176002(Rr)', '2.00', 60000, '1.00', 1),
(28, 'FM285TH', 'SZ31176002(Rw)', '2.00', 60000, '1.00', 1),
(29, 'FM285TH', 'SZ36650014L', '2.00', 60000, '1.00', 1),
(30, 'FM285TH', 'SZ366JAA10(Rr)', '2.00', 60000, '1.00', 1),
(31, 'FM285TH', 'SZ366JAA10(Rw)', '2.00', 60000, '1.00', 1),
(32, 'FM285TH', 'SZ366JFA11(Rr)', '2.00', 60000, '1.00', 1),
(33, 'FM285TH', 'SZ366JFA11(Rw)', '2.00', 60000, '1.00', 1),
(34, 'FM285TH', 'SZ37125045', '1.00', 60000, '1.00', 1),
(35, 'FM285TH', 'SZ91049333', '1.00', 120000, '1.00', 1),
(36, 'FM285TH', 'SZ93021100', '2.00', 60000, '1.00', 1),
(37, 'FM285TH', 'SZ98032070', '1.00', 60000, '0.50', 1),
(38, 'FM285TH', 'SZ98032071', '1.00', 60000, '0.50', 1),
(39, 'FM285TH', 'SZ980T7001(L)', '1.00', 60000, '0.50', 1),
(40, 'FM285TH', 'SZ980T7001(R)', '1.00', 60000, '0.50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `unitmaster`
--

CREATE TABLE `unitmaster` (
  `unit_id` int(11) NOT NULL,
  `unit_type` varchar(20) NOT NULL,
  `unit_name` varchar(50) NOT NULL,
  `valuable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unitmaster`
--

INSERT INTO `unitmaster` (`unit_id`, `unit_type`, `unit_name`, `valuable`) VALUES
(1, 'FM285TH', 'FM 285 TH', 1),
(2, 'FM260JD', 'FM 260 JD', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_general`
--
ALTER TABLE `inv_general`
  ADD PRIMARY KEY (`inv_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `manpower`
--
ALTER TABLE `manpower`
  ADD PRIMARY KEY (`mp_id`);

--
-- Indexes for table `partmaster_mtc`
--
ALTER TABLE `partmaster_mtc`
  ADD PRIMARY KEY (`partmtc_id`);

--
-- Indexes for table `partmaster_rpr`
--
ALTER TABLE `partmaster_rpr`
  ADD PRIMARY KEY (`partrpr_id`);

--
-- Indexes for table `part_mtc`
--
ALTER TABLE `part_mtc`
  ADD PRIMARY KEY (`typemtc_id`);

--
-- Indexes for table `part_rpr`
--
ALTER TABLE `part_rpr`
  ADD PRIMARY KEY (`typerpr_id`);

--
-- Indexes for table `unitmaster`
--
ALTER TABLE `unitmaster`
  ADD PRIMARY KEY (`unit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_general`
--
ALTER TABLE `inv_general`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `manpower`
--
ALTER TABLE `manpower`
  MODIFY `mp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `partmaster_mtc`
--
ALTER TABLE `partmaster_mtc`
  MODIFY `partmtc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `partmaster_rpr`
--
ALTER TABLE `partmaster_rpr`
  MODIFY `partrpr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `part_mtc`
--
ALTER TABLE `part_mtc`
  MODIFY `typemtc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `part_rpr`
--
ALTER TABLE `part_rpr`
  MODIFY `typerpr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `unitmaster`
--
ALTER TABLE `unitmaster`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
